package com.sefonsoft.hive;


import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.security.UserGroupInformation;
//import org.apache.hive.hcatalog.api.HCatClient;
import org.apache.hive.hcatalog.common.HCatException;
import org.apache.hive.hcatalog.data.DefaultHCatRecord;
import org.apache.hive.hcatalog.data.HCatRecord;
import org.apache.hive.hcatalog.data.schema.HCatSchema;
import org.apache.hive.hcatalog.data.transfer.DataTransferFactory;
import org.apache.hive.hcatalog.data.transfer.HCatWriter;
import org.apache.hive.hcatalog.data.transfer.WriteEntity;
import org.apache.hive.hcatalog.data.transfer.WriterContext;
//import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

//import static org.junit.Assert.*;

/**
 * 此方案 暂未 测试通过
 *
 */
public class HiveUtilsTest {

//    @Test
    public void writeEntity() throws HCatException {
       /* Properties prop = new Properties();
        String basePath = "E:\\idea_workspace\\hh_etl\\kettle-webapp\\src\\main\\resources\\hive";
        try {
            prop.load(new FileInputStream(basePath + "\\" + "hive.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        try {
            String basePath = "E:\\idea_workspace\\hh_etl\\kettle-webapp\\src\\main\\resources\\hive";
            String metaStoreUri = "thrift://10.0.10.16:9083";
//            String metaStoreUri = "thrift://10.0.10.13:9083";
            String principal ="hive/sdcetl.sefonsoft.com@SEFON.COM";
//            String principal ="hive/sdc13.sefonsoft.com@SEFON.COM";
            String keytab = basePath + "\\hive.service.keytab";

            System.setProperty("sun.security.krb5.debug", "true");
            System.setProperty("java.security.krb5.conf", basePath+"\\..\\krb5.conf");
            Configuration config = new Configuration();
            config.set(HiveConf.ConfVars.METASTOREURIS.toString(), metaStoreUri);
            config.set("hadoop.security.authentication", "kerberos");
            config.set(HiveConf.ConfVars.METASTORE_KERBEROS_PRINCIPAL.toString(), principal);
            config.set(HiveConf.ConfVars.METASTORE_KERBEROS_KEYTAB_FILE.toString(), keytab);
            config.set(HiveConf.ConfVars.METASTORE_USE_THRIFT_SASL.toString(), "true");
            config.set("hcatalog.hive.client.cache.disabled", "false");
            config.set("hive.metastore.execute.setugi", "false");
            config.set("hive.metastore.uris", metaStoreUri);
            UserGroupInformation.setConfiguration(config);
            UserGroupInformation.loginUserFromKeytab(principal, keytab);

            //需要增加hadoop开启了安全的配置
            //配置安全认证方式为kerberos

            HashMap<String, String> configMap = new HashMap<>();
            configMap.put("hive.metastore.uris", metaStoreUri);
            configMap.put("yarn.resourcemanager.principal", "yarn/sdc11.sefonsoft.com@SEFON.COM");

//            HCatClient client = HCatClient.create(config);

            HashMap<String, String> partitionMap = new HashMap<>();
            partitionMap.put("time","17-12-12-00-00");

            WriteEntity.Builder builder = new WriteEntity.Builder();
            WriteEntity entity = builder.withDatabase("wx_11").withTable("test7").withPartition(partitionMap).build();

            ArrayList<HCatRecord> hcrList = new ArrayList<>();
            HCatRecord hcr = new DefaultHCatRecord(2);
            hcr.set(0, 1);
            hcr.set(1, "wenxin");
//            hcr.set(2, "17-12-12-00-00");
            HCatRecord hcr2 = new DefaultHCatRecord(2);
            hcr2.set(0, 2);
            hcr2.set(1, "wenxin2");
//            hcr2.set(2, "17-12-13-00-00");
            hcrList.add(hcr);
            hcrList.add(hcr2);

            HCatWriter writer = DataTransferFactory.getHCatWriter(entity, configMap);
            WriterContext info = writer.prepareWrite();
            HCatWriter writers = DataTransferFactory.getHCatWriter(info);
            writers.write(hcrList.iterator());
            writer.commit(info);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
